# C++ Program: Hash Table


## Program Details
### Purpose
Practice with **Hash Tables**.

Hash table structures offer an ideal method of searching for data efficently. In this case, key-value pair data is obtained from the `data.csv` file, then said data is pigeonholed into the appropriate table index. The `data.csv` holds 13 key-value pairs.

A generated ***HashTable*** object has a table of size 19, and can theoredically accept any number of key-value pairs. To deal with collisions, a separate chaining solution is applied allowing for *linked lists* to exist in the table if necessary.

The chosen hash function in this particular instance is a simple modulus equation. In this case, a three-digit key is divided by the size of the table. The remaider of this dividend/key is what determines which table index the key-value pair is stored in. This equation is too simple, but serves as a great way to model and learn from.

### Relevant Data Structures, Algorithms, or Library Functions
- One library function for string manipulation was applied.**stoi()**. After extracting the string data from the `.csv` file, this library function ensures adequate preparation of some strings for the table structure.

- A **Modular Programming** methodology is used for this program. Code is separated accross different files. 

- **Object-Oriented Progamming**. The table itself is an array within a ***HashTable*** object that holds ***List*** class instances per array/table index. Every ***List*** instance can potentially hold many struct ***Node*** instances. Its important to note that **Pointer** logic makes this dynamic structure work as intended. 

- Performance metrics:
    
    - **Time Complexity**. How does input influence time?
     
        The same time complexity is shared for a data search and deletion within the table. When adding a new node to the table, ideally, only the hash function operation determines the bucket where the value should be stored. No other necessary movements would be made within the table to store, search, or delete a node thus avoiding any prolonged procedural delay. Being so, the time complexity for a best case scenario is the coveted ***Constant*** time complexity.

        <br/>

        ![BigoTsb](images/table_bigOconst.png)

        <br/>

        With regards to a table implementing separate chaining for collisions, a worst case scenario would only occur if it utilizes a bad hash function. If for some magical reason all given key-value pairs ***n*** are hashed into the same table index, then the entire table structure would reduce itself to the time complexity of a linked-list or a simple sequential array. Though unlikely, this worst case scenario would produce a ***Linear*** time complexity for the table. 

        <br/>

        ![BigoLinear](images/table_bigOtimeSpace.png)

        <br/>

    - **Space Complexity**. How does input influence space?
  
        Memory used by this table structure as it grows from the number ***n*** of stored key-value pairs leads to a ***Linear*** space complexity. This is the case weather there are collisions or not.

        <br/>

        ![BigoLinear](images/table_bigOtimeSpace.png)
        <br/>

## Program Installation
One can obtain an environment to work with C++ using certain IDE's such as **CLion**, **Eclipse**, or **Code::Blocks**. Others such as**Visual Studio**, require pluggins to handle "C/C++". The chosen option for the C++ program shown here were the **Visual Studio** puggins. The following link has directions on how to properly set up this environment: https://learn.microsoft.com/en-us/cpp/build/vscpp-step-0-installation?view=msvc-170

With the C++ compiler installed, the quickest way to run this program is by simply copying and pasting the entire `.cpp` code into a new personal `.cpp` file. Save the new file. Push the "Run" button and done. Otherwise, a command terminal can be opened where the personal `.cpp` file exists, and the following commands can be entered to run the application:
```console
g++ -o  executable myFileNameHere.cpp
```
```console
.\executable
```

## Use Case/Example Output
```c++
-------------------------------------------------------------------------------------
|                             TEST: The Empty Hash Table                            |
-------------------------------------------------------------------------------------

Empty Table?:       Yes
Number Of Elements: 0
       SearchKey    |   Data
- - - - - - - - - - - - - - - - - -
Cell 0:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 1:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 2:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 3:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 4:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 5:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 6:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 7:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 8:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 9:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell10:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell11:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell12:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell13:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell14:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell15:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell16:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell17:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell18:xxxxxxxxxxxxxxxxxxxxxxxxxxxx

...searching for, "001", a non-existing search key. Should return "No": No

...searching for data belonging to search key "001". Should return a blank string:

...removing data related to search key "001". Should return "No": No

...clearing an already empty hash table.

                                 Data After Operations
                             - - - - - - - - - - - - - - -
Empty Table?:       Yes
Number Of Elements: 0
       SearchKey    |   Data
- - - - - - - - - - - - - - - - - -
Cell 0:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 1:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 2:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 3:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 4:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 5:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 6:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 7:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 8:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 9:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell10:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell11:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell12:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell13:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell14:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell15:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell16:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell17:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell18:xxxxxxxxxxxxxxxxxxxxxxxxxxxx


-------------------------------------------------------------------------------------
|                             TEST: A Filled Hash Table                             |
-------------------------------------------------------------------------------------

...adding file data to the hash table.
13 entries added.

                                 Data After Operations
                             - - - - - - - - - - - - - - -
Empty Table?:       No
Number Of Elements: 13
       SearchKey    |   Data
- - - - - - - - - - - - - - - - - -
Cell 0:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 1:     115 - - -> eleventh
Cell 2:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 3:     117 - - -> third
            155 - - -> seventh
Cell 4:     156 - - -> ninth
Cell 5:     100 - - -> first
            157 - - -> fifth
            195 - - -> tenth
Cell 6:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 7:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 8:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 9:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell10:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell11:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell12:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell13:     108 - - -> second
Cell14:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell15:     129 - - -> fourth
            205 - - -> sixth
Cell16:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell17:     131 - - -> eighth
Cell18:     208 - - -> twelfth
            132 - - -> thirteenth

...searching for search key, "100". Should return "Yes": Yes

...searching for data belonging to search key "100". Should return "first": first

...searching for search key, "157". Should return "Yes": Yes

...searching for data belonging to search key "157". Should return "fifth": fifth

...searching for search key, "195". Should return "Yes": Yes

...searching for data belonging to search key "195". Should return "tenth": tenth

                                 Data After Operations
                             - - - - - - - - - - - - - - -
Empty Table?:       No
Number Of Elements: 13
       SearchKey    |   Data
- - - - - - - - - - - - - - - - - -
Cell 0:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 1:     115 - - -> eleventh
Cell 2:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 3:     117 - - -> third
            155 - - -> seventh
Cell 4:     156 - - -> ninth
Cell 5:     100 - - -> first
            157 - - -> fifth
            195 - - -> tenth
Cell 6:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 7:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 8:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 9:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell10:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell11:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell12:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell13:     108 - - -> second
Cell14:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell15:     129 - - -> fourth
            205 - - -> sixth
Cell16:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell17:     131 - - -> eighth
Cell18:     208 - - -> twelfth
            132 - - -> thirteenth

...removing, "131", an entry with no collisions. Should return "Yes": Yes

...removing, "132", an entry that collided once. Should return "Yes": Yes

...removing, "195", an entry that collided twice. Should return "Yes": Yes

                                 Data After Operations
                             - - - - - - - - - - - - - - -
Empty Table?:       No
Number Of Elements: 10
       SearchKey    |   Data
- - - - - - - - - - - - - - - - - -
Cell 0:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 1:     115 - - -> eleventh
Cell 2:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 3:     117 - - -> third
            155 - - -> seventh
Cell 4:     156 - - -> ninth
Cell 5:     100 - - -> first
            157 - - -> fifth
Cell 6:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 7:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 8:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 9:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell10:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell11:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell12:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell13:     108 - - -> second
Cell14:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell15:     129 - - -> fourth
            205 - - -> sixth
Cell16:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell17:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell18:     208 - - -> twelfth

...adding entry to index 5 of the table where an entry was previously deleted. 1 entry added.

...adding entry to index 18 of the table where an entry was previously deleted. 1 entry added.

...adding entry to index 17 of the table where an entry was previously deleted. 1 entry added.

                                 Data After Operations
                             - - - - - - - - - - - - - - -
Empty Table?:       No
Number Of Elements: 13
       SearchKey    |   Data       
- - - - - - - - - - - - - - - - - -
Cell 0:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 1:     115 - - -> eleventh
Cell 2:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 3:     117 - - -> third
            155 - - -> seventh
Cell 4:     156 - - -> ninth
Cell 5:     100 - - -> first
            157 - - -> fifth
            138 - - -> doggyDog
Cell 6:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 7:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 8:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 9:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell10:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell11:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell12:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell13:     108 - - -> second
Cell14:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell15:     129 - - -> fourth
            205 - - -> sixth
Cell16:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell17:     150 - - -> fishyFish
Cell18:     208 - - -> twelfth
            151 - - -> kittyCat

...removing, "157", an entry sandwiched between two other enries. Should return "Yes": Yes

                                 Data After Operations
                             - - - - - - - - - - - - - - -
Empty Table?:       No
Number Of Elements: 12
       SearchKey    |   Data
- - - - - - - - - - - - - - - - - -
Cell 0:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 1:     115 - - -> eleventh
Cell 2:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 3:     117 - - -> third
            155 - - -> seventh
Cell 4:     156 - - -> ninth
Cell 5:     100 - - -> first
            138 - - -> doggyDog
Cell 6:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 7:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 8:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 9:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell10:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell11:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell12:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell13:     108 - - -> second
Cell14:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell15:     129 - - -> fourth
            205 - - -> sixth
Cell16:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell17:     150 - - -> fishyFish
Cell18:     208 - - -> twelfth
            151 - - -> kittyCat

...clearing a filled hash table.

                                 Data After Operations
                             - - - - - - - - - - - - - - -
Empty Table?:       Yes
Number Of Elements: 0
       SearchKey    |   Data
- - - - - - - - - - - - - - - - - -
Cell 0:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 1:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 2:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 3:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 4:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 5:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 6:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 7:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 8:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 9:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell10:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell11:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell12:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell13:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell14:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell15:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell16:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell17:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell18:xxxxxxxxxxxxxxxxxxxxxxxxxxxx

...adding same file data to the empty hash table again. 13 entries added.

                                 Data After Operations
                             - - - - - - - - - - - - - - -
Empty Table?:       No
Number Of Elements: 13
       SearchKey    |   Data
- - - - - - - - - - - - - - - - - -
Cell 0:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 1:     115 - - -> eleventh
Cell 2:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 3:     117 - - -> third
            155 - - -> seventh
Cell 4:     156 - - -> ninth
Cell 5:     100 - - -> first
            157 - - -> fifth
            195 - - -> tenth
Cell 6:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 7:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 8:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell 9:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell10:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell11:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell12:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell13:     108 - - -> second
Cell14:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell15:     129 - - -> fourth
            205 - - -> sixth
Cell16:xxxxxxxxxxxxxxxxxxxxxxxxxxxx
Cell17:     131 - - -> eighth
Cell18:     208 - - -> twelfth
            132 - - -> thirteenth
```