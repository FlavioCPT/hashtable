/* ***********************************************************************************************
 * Name: Flavio Palacios                                                                         *
 * Purpose: Dictionary implementation                                                            *
 * Notes: "HashTable" class functions are defined here.                                          *
 * ***********************************************************************************************/
#include "dictionary.h"

//- - - - - - - - - - - - - - - -
//   ***IS THE TABLE EMPTY?***
//- - - - - - - - - - - - - - - -
bool HashTable::isEmpty(){
    bool emptyIndex=true;
    //Every index in the "table" array holds a different instance of the "List" class.
    for(int index=0;index<SIZE;++index){
        if(table[index]->getCount()!=0){emptyIndex=false;};
    };

    if(emptyIndex==true){return true;}
    else{return false;};
}

//- - - - - - - - - - - - - - - - - - - - -
//   ***COUNTS NODES IN THE TABLE***
//- - - - - - - - - - - - - - - - - - - - -
int HashTable::getNumberOfItems(){
    int accumulator=0;

    //Every "Node" instance within every "List" instance is counted.
    for(int index=0;index<SIZE;++index){
        if(table[index]->getCount()!=0){
            accumulator=accumulator+table[index]->getCount();
        };
    };

    return accumulator;
}

//- - - - - - - - - - - - - - -
//   ***ADD DATA TO TABLE***
//- - - - - - - - - - - - - - -
void HashTable::add(int givenKey,string givenString){
    int hashedArrayIndex;
    struct Node *newNode;

    //"newNode" pointer stores given key-value pair.
    newNode=new Node(givenKey,givenString);

    //HASH FUNCTION: Modulus hash.
    hashedArrayIndex=givenKey%SIZE;
    //The "List" class instance in this cell is the one that does the work for adding this node
    //to the table. It does so using the "List" class "addNode()" function.
    table[hashedArrayIndex]->addNode(newNode);
}

//- - - - - - - - - - - - - - - - - - -
//   ***LOCATE ITEM IN THE TABLE***
//- - - - - - - - - - - - - - - - - - -
bool HashTable::contains(int givenSearchKey){
    //Given search key is hashed to see where in the "table" array the key-value pair shall go.
    int hashedArrayIndex=givenSearchKey%SIZE;

    //In the appropriate "table" index, the "List" class "findNode()" is called.
    //If the function returns a pointer to NULL then the "givenSearchKey" does not exist.
    struct Node *elementFinderPtr=table[hashedArrayIndex]->findNode(givenSearchKey);

    if(elementFinderPtr!=nullptr){return true;}
    else{return false;};
}

//- - - - - - - - - - - - - - - - -
//   ***DELETE TABLE ELEMENT***
//- - - - - - - - - - - - - - - - -
bool HashTable::remove(int givenSearchKey){
    //A search similar to the "contains()" function is made.
    bool removalSuccess;
    int hashedArrayIndex=givenSearchKey%SIZE;
    struct Node *elementFinderPtr=table[hashedArrayIndex]->findNode(givenSearchKey);

    //If the "Node" returned by the "List" class' "findNode()" function is not NULL, then it is deleted
    //using the "List" class "deleteNode()" member function.
    if(elementFinderPtr!=nullptr){removalSuccess=(table[hashedArrayIndex]->deleteNode(givenSearchKey));}
    else{removalSuccess==false;};

    return removalSuccess;
}

//- - - - - - - - - - - - - - - - - - - - - - - - -
//   ***LOCATE A STRING ELEMENT IN THE TABLE***
//- - - - - - - - - - - - - - - - - - - - - - - - -
string HashTable::getItem(int givenSearchKey){
    //A search similar to the "contains()" function is made.
    int hashedArrayIndex=givenSearchKey%SIZE;
    struct Node *elementFinderPtr=table[hashedArrayIndex]->findNode(givenSearchKey);

    //After using "List" class' "findNode()" function, the returned node is scavenged for its string value.
    //If "findNode()" returns NULL, then an empty string is returned.
    string desiredData;
    if(elementFinderPtr!=nullptr){
        desiredData=elementFinderPtr->data;
    }else{
        desiredData="";
    };

     return desiredData;
}

//- - - - - - - - - - - - - - - - - - -
//   ***DELETE ALL TABLE ELEMENTS***
//- - - - - - - - - - - - - - - - - - -
void HashTable::clear(){
    //For every index in the hash table, the "List" class "clearList()" function is called.
    for(int index=0;index<SIZE;index++){
        table[index]->clearList();
    };
}

//- - - - - - - - - - - - - - - - -
//   ***TABLE TRAVERSAL(PRINT INFO)***
//- - - - - - - - - - - - - - - - -
void HashTable::traverse(){
    cout<<"       SearchKey    |   Data       "<<endl;
    cout<<"- - - - - - - - - - - - - - - - - -"<<endl;

    for(int index=0;index<SIZE;index++) {
        //This prints whitespace filler "x" for empty cells in the table.
        if(table[index]->getCount()==0){
            if(index<10){cout<<"Cell "<<index<<":xxxxxxxxxxxxxxxxxxxxxxxxxxxx"<<endl;}
            else{cout<<"Cell"<<index<<":xxxxxxxxxxxxxxxxxxxxxxxxxxxx"<<endl;}
        }else{
            //This prints data for cells with node entries using the "List" class "printList()" function.
            if(index<10){
                cout<<"Cell "<<index<<":";
                table[index]->printList();
            }
            else{
                cout<<"Cell"<<index<<":";
                table[index]->printList();
            };
        };
    };
}

//- - - - - - - - - - - -
//   ***CONSTRUCTORS***
//- - - - - - - - - - - -
HashTable::HashTable(){
    //"List" instances are created in every "table" cell.
    for(int index=0;index<SIZE;++index){
        table[index]=new List;
    };
}

