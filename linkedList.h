/* ***********************************************************************************************
 * Name: Flavio Palacios                                                                         *
 * Purpose: Linked list header                                                                   *
 * Notes: "Node" and "List" class declarations.                                                  *
 *************************************************************************************************/
#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <iostream>
#include <string>
using namespace std;

//"Nodes" will be instances inside the "List" class.
struct Node{
    int searchKey;
    string data;
    Node *forward;

    Node(int,string);  //OVERLOADED CONSTRUCTOR.
};

//"Lists" will instantiate for every "HashTable" index.
class List{
private:
    int count;  //Node counter.
    struct Node *headptr;  //Points to start.
    struct Node *tailptr;  //Points to finish.

public:
    List();  //CONSTRUCTOR.

    bool addNode(struct Node*);  //Adds new node.

    bool deleteNode(int);  //Delete node given "searchKey".

    Node *findNode(int);  //Find node given "searchKey".

    void clearList();  //Delete all nodes in list.

    void printList();  //Print all nodes in list.

    int getCount();
};
#endif //LINKEDLIST_H