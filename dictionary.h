/* ***********************************************************************************************
 * Name: Flavio Palacios                                                                         *                                                                       *
 * Purpose: Dictionary header                                                                    *                                                                                     
 * Notes: "HashTable" class declaration. The hash table itself is the declared "table" array     *
 * which holds "List" class instances.                                                           *
 * ***********************************************************************************************/
#ifndef DICTIONARY_H
#define DICTIONARY_H

#include "linkedList.h"

class HashTable{
private:
    static const int SIZE=19;
    List *table[SIZE];  //Hash table that holds "List" instances. 

public:
    HashTable();  //CONSTRUCTOR:Creates "List" instances in every "table" cell.

    bool isEmpty();  //"true" if empty. "false" if not empty.

    int getNumberOfItems();  //Counts existing key-value pairs.

    void add(int,string);  //Store given key-value pair into hash table.

    bool contains(int);  //Find desired key. "true"=found, "false"=not found.

    bool remove(int);  //Delete desired key-value pair. "true"=deleted, "false"=not deleted.

    string getItem(int);  //Given search key, return string value.

    void clear();  //Clear all "Node" instances from "table" 

    void traverse();  //Print the hash table.
};
#endif //DICTIONARY_H