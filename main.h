/* ***********************************************************************************************
 * Name: Flavio Palacios                                                                         *
 * Purpose: Main program header                                                                  *
 * Notes: Declarations for functions used in "main.cpp".                                         *
 * ***********************************************************************************************/
#ifndef MAIN_H
#define MAIN_H

#include "dictionary.h"
#include <cstring>  //For "strlen()".   DELETE??
#include <cstdlib>  ///DELETE LINE?
#include <iomanip>  //For "left" and "setw()".
#include <fstream>  //For file operations.




string successEcho(bool);  //Converts bool to a string. true="Yes", false="No".


int findFileRows();  //Finds rows in ".csv" file. Rows will equal hash table size.


string *storeFileDataInArray(string [], int);  //Passes an array by reference and its corresponding
                                               //size to store the ".csv" file's contents.

#endif //MAIN_H
