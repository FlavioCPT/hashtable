/* ***********************************************************************************************
 * Name: Flavio Palacios                                                                         *
 * Purpose: Main program                                                                         *
 * Notes: This is the testing ground for the constructed "HashTable" class. Instead of using     *
 *        linear probing for collisions, separate chaining is applied. File operations are       *
 *        performed on the "data.csv" file to extract its key-value pair data.                   *
 * ***********************************************************************************************/

#include "functions.cpp"
#include "dictionary.cpp"
#include "linkedList.cpp"

int main(){
    HashTable hashTable;  //Hash table object generated.

    //Operations are tested for an empty hash table.
    cout<<setw(85)<<left<<"-------------------------------------------------------------------------------------"<<endl;
    cout<<setw(85)<<left<<"|                             TEST: The Empty Hash Table                            |"<<endl;
    cout<<setw(85)<<left<<"-------------------------------------------------------------------------------------"<<endl<<endl;
    cout<<"Empty Table?:       "<<successEcho(hashTable.isEmpty())<<endl;
    cout<<"Number Of Elements: "<<hashTable.getNumberOfItems()<<endl;
    hashTable.traverse();
    cout<<endl;

    cout<<"...searching for, \"001\", a non-existing search key. Should return \"No\": "<<successEcho(hashTable.contains(001))<<endl<<endl;
    cout<<"...searching for data belonging to search key \"001\". Should return a blank string:"<< hashTable.getItem(001)<<endl<<endl;
    cout<<"...removing data related to search key \"001\". Should return \"No\": " << successEcho(hashTable.remove(001))<<endl<<endl;
    cout<<"...clearing an already empty hash table."<<endl<<endl;
    hashTable.clear();
    cout<<setw(85)<<left<<"                                 Data After Operations                               "<<endl;
    cout<<setw(85)<<left<<"                             - - - - - - - - - - - - - - -                           "<<endl;
    cout<<"Empty Table?:       "<<successEcho(hashTable.isEmpty())<<endl;
    cout<<"Number Of Elements: "<<hashTable.getNumberOfItems()<<endl;
    hashTable.traverse();
    cout<<endl<<endl;

    //Operations are tested on a table filled with csv file key-value pairs.
    cout<<setw(85)<<left<<"-------------------------------------------------------------------------------------"<<endl;
    cout<<setw(85)<<left<<"|                             TEST: A Filled Hash Table                             |"<<endl;
    cout<<setw(85)<<left<<"-------------------------------------------------------------------------------------"<<endl<<endl;
    const int ARRAY_SIZE=findFileRows();  //Traverse csv file to count rows. "ARRAY_SIZE=(rows*2)+1".
    string array[ARRAY_SIZE];  //Array for holding file contents.

    string *arrayPtr=storeFileDataInArray(array,ARRAY_SIZE);  //Store key-value pairs in array:
                                                              //[0] holds "array" size.
                                                              //[odd index] holds keys. Key's value is the right adjacent index.
                                                              //[even index] holds values. Value's key is the left adjacent index.

    cout<<"...adding file data to the hash table. "<<endl;
    for(int i=1;i<stoi(*(arrayPtr));i++){  //"arrayPtr" points to "array". "array"'s values are hashed.
        hashTable.add(stoi(*(arrayPtr+i)),*(arrayPtr+(i+1)));
        i++;
    };

    cout<<(stoi(*(arrayPtr))-1)/2<<" entries added."<<endl<<endl;
    cout<<setw(85)<<left<<"                                 Data After Operations                               "<<endl;
    cout<<setw(85)<<left<<"                             - - - - - - - - - - - - - - -                           "<<endl;
    cout<<"Empty Table?:       "<<successEcho(hashTable.isEmpty())<<endl;
    cout<<"Number Of Elements: "<<hashTable.getNumberOfItems()<<endl;
    hashTable.traverse();
    cout<<endl;

    cout<<"...searching for search key, \"100\". Should return \"Yes\": "<<successEcho(hashTable.contains(100))<<endl<<endl;
    cout<<"...searching for data belonging to search key \"100\". Should return \"first\": "<<hashTable.getItem(100)<<endl<<endl;
    cout<<"...searching for search key, \"157\". Should return \"Yes\": "<<successEcho(hashTable.contains(157))<<endl<<endl;
    cout<<"...searching for data belonging to search key \"157\". Should return \"fifth\": "<<hashTable.getItem(157)<<endl<<endl;
    cout<<"...searching for search key, \"195\". Should return \"Yes\": "<<successEcho(hashTable.contains(195))<<endl<<endl;
    cout<<"...searching for data belonging to search key \"195\". Should return \"tenth\": "<<hashTable.getItem(195)<<endl<<endl;
    cout<<setw(85)<<left<<"                                 Data After Operations                               "<<endl;
    cout<<setw(85)<<left<<"                             - - - - - - - - - - - - - - -                           "<<endl;
    cout<<"Empty Table?:       "<<successEcho(hashTable.isEmpty())<<endl;
    cout<<"Number Of Elements: "<<hashTable.getNumberOfItems()<<endl;
    hashTable.traverse();
    cout<<endl;

    cout<<"...removing, \"131\", an entry with no collisions. Should return \"Yes\": "<<successEcho(hashTable.remove(131))<<endl<<endl;
    cout<<"...removing, \"132\", an entry that collided once. Should return \"Yes\": "<<successEcho(hashTable.remove(132))<<endl<<endl;
    cout<<"...removing, \"195\", an entry that collided twice. Should return \"Yes\": "<<successEcho(hashTable.remove(195))<<endl<<endl;
    cout<<setw(85)<<left<<"                                 Data After Operations                               "<<endl;
    cout<<setw(85)<<left<<"                             - - - - - - - - - - - - - - -                           "<<endl;
    cout<<"Empty Table?:       "<<successEcho(hashTable.isEmpty())<<endl;
    cout<<"Number Of Elements: "<<hashTable.getNumberOfItems()<<endl;
    hashTable.traverse();
    cout<<endl;

    cout<<"...adding entry to index 5 of the table where an entry was previously deleted. ";
    hashTable.add(138,"doggyDog");
    cout<<1<<" entry added."<<endl<<endl;
    cout<< "...adding entry to index 18 of the table where an entry was previously deleted. ";
    hashTable.add(151,"kittyCat");
    cout<<1<<" entry added."<<endl<<endl;
    cout<<"...adding entry to index 17 of the table where an entry was previously deleted. ";
    hashTable.add(150,"fishyFish");
    cout<<1<<" entry added."<<endl<<endl;

    cout<<setw(85)<<left<<"                                 Data After Operations                               "<<endl;
    cout<<setw(85)<<left<<"                             - - - - - - - - - - - - - - -                           "<<endl;
    cout<<"Empty Table?:       "<<successEcho(hashTable.isEmpty())<<endl;
    cout<<"Number Of Elements: "<<hashTable.getNumberOfItems()<<endl;
    hashTable.traverse();
    cout<<endl;

    cout<<"...removing, \"157\", an entry sandwiched between two other enries. Should return \"Yes\": " << successEcho(hashTable.remove(157))<<endl<<endl;
    cout<<setw(85)<<left<<"                                 Data After Operations                               "<<endl;
    cout<<setw(85)<<left<<"                             - - - - - - - - - - - - - - -                           "<<endl;
    cout<<"Empty Table?:       "<<successEcho(hashTable.isEmpty())<<endl;
    cout<<"Number Of Elements: "<<hashTable.getNumberOfItems()<<endl;
    hashTable.traverse();
    cout<<endl;

    cout<<"...clearing a filled hash table."<<endl<<endl;
    hashTable.clear();
    cout<<setw(85)<<left<<"                                 Data After Operations                               "<<endl;
    cout<<setw(85) << left << "                             - - - - - - - - - - - - - - -                           "<<endl;
    cout<<"Empty Table?:       "<<successEcho(hashTable.isEmpty())<<endl;
    cout<<"Number Of Elements: "<<hashTable.getNumberOfItems()<<endl;
    hashTable.traverse();
    cout<<endl;

    cout<<"...adding same file data to the empty hash table again. ";
    for(int i=1;i<stoi(*(arrayPtr));i++){
        hashTable.add(stoi(*(arrayPtr+i)),*(arrayPtr+(i+1)));
        i++;
    };

    cout<<(stoi(*(arrayPtr))-1)/2<<" entries added."<<endl<<endl;
    cout<<setw(85)<<left<<"                                 Data After Operations                               "<<endl;
    cout<<setw(85)<<left<<"                             - - - - - - - - - - - - - - -                           "<<endl;
    cout<<"Empty Table?:       "<<successEcho(hashTable.isEmpty())<<endl;
    cout<<"Number Of Elements: "<<hashTable.getNumberOfItems()<<endl;
    hashTable.traverse();
    cout<<endl;

    return 0;
}