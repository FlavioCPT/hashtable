/* ***********************************************************************************************
 * Name: Flavio Palacios                                                                         *
 * Purpose: Linked list implementation                                                           *
 * Notes: "Node" and "List" class definitions.                                                   *
 *************************************************************************************************/
#include "linkedList.h"

//- - - - - - - - - - - - - - - -
//   ***ACCESSOR FOR COUNT***
//- - - - - - - - - - - - - - - -
int List::getCount(){
    return count;
}

//- - - - - - - - - - - - - - - - - -
//   ***ADD A NODE TO THE LIST***
//- - - - - - - - - - - - - - - - - -
bool List::addNode(struct Node *givenNode){
    count++;  //Update "count".

    if(givenNode==nullptr){
        count--;
        return false;
    }

    //When there's no nodes, the head and tail pointer aim at the new node
    //and their "forward" pointers point to NULL. Else, when
    //there's at least one node, the new node will become the tail.
    else if(headptr==nullptr){
        headptr=givenNode;
        tailptr=givenNode;
    }
    else{
        tailptr->forward=givenNode;
        tailptr=givenNode;
    };

    return true;
}

//- - - - - - - - - - - - - - - - - - -
//   ***DELETE NODE FROM THE LIST***
//- - - - - - - - - - - - - - - - - - -
bool List::deleteNode(int givenNodeValue){
    struct Node *delNodePtr=headptr;  //Pointer for list traversal.
    bool success=true;

    //If nothing found.
    if(headptr==nullptr){
        success=false;
        cout<<"There are no nodes to delete."<<endl;
    }

    //If the lonely head node is to be deleted.
    else if((headptr->forward==nullptr)&&(givenNodeValue==headptr->searchKey)){
        delete delNodePtr;
        headptr=nullptr;
        count--;
    }

    //If the head node is deleted, but there is at least one node in sequence.
    else if((headptr->forward!=nullptr)&&(givenNodeValue==headptr->searchKey)){
        headptr=headptr->forward;
        delete delNodePtr;
        count--;
    }

    //If the tail node is to be deleted.
    else if(givenNodeValue==tailptr->searchKey){
        while(delNodePtr->forward!=tailptr){
            delNodePtr=delNodePtr->forward;
        };
        tailptr=delNodePtr;
        tailptr->forward=nullptr;
        delNodePtr=delNodePtr->forward;
        delete delNodePtr;
        count--;
    }

    //Else delete a node in between the head and tail node.
    else{
        //"tempptr" will traverse the list and point to the node that comes
        //before the node pointed to by "delNodePtr". "tempptrTwo" will point
        //to the node that "delNodePtr->forward" points to.
        struct Node *tempptr=headptr;
        struct Node *tempptrTwo;

        delNodePtr=delNodePtr->forward;
        while((givenNodeValue!=delNodePtr->searchKey)&&(delNodePtr!=nullptr)){
            tempptr=delNodePtr;
            delNodePtr=delNodePtr->forward;
        };
        if(delNodePtr==nullptr){success=false;}
        else{
            tempptrTwo=delNodePtr->forward;
            tempptr->forward=tempptrTwo;
            delete delNodePtr;
            count--;
        };
    };

    if(success==false){return false;}
    else{return true;};
}

//- - - - - - - - - - - -
//   ***FIND A NODE***
//- - - - - - - - - - - -
Node *List::findNode(int givenId){  
    struct Node *traverserPtr=headptr;  //Pointer for list traversal.

    //The "traverserPtr" will keep moving forward into the list until it
    //finds a node "id" to match the "givenId", or until it reaches NULL.
    while((traverserPtr!=nullptr)&&(givenId!=traverserPtr->searchKey)){
        traverserPtr=traverserPtr->forward;
    };

    return traverserPtr;
}

//- - - - - - - - - - - - - - - - - - -
//   ***DELETE ALL LIST ELEMENTS***
//- - - - - - - - - - - - - - - - - - -
void List::clearList(){
    if(headptr==nullptr){
        //Do nothing here.
    }else{
        struct Node *deletionPtr=headptr;
        while(headptr!=nullptr){
            headptr=headptr->forward;
            delete deletionPtr;
            deletionPtr=headptr;
            count--;
        };

        tailptr=headptr;
    };
}

//- - - - - - - - - - - - - - - - -
//   ***PRINT ALL LIST NODES***
//- - - - - - - - - - - - - - - - -
void List::printList(){
    struct Node *traverserPtr=headptr; //Pointer for list traversal.
    while(traverserPtr!=nullptr){
        if(traverserPtr==headptr){cout<<"     "<<traverserPtr->searchKey<<" - - -> "<<traverserPtr->data<<endl;}
        else{cout<<"            "<<traverserPtr->searchKey<<" - - -> "<<traverserPtr->data<<endl;};
        traverserPtr=traverserPtr->forward;
    };
}

//- - - - - - - - - - - -
//   ***CONSTRUCTORS***
//- - - - - - - - - - - -
List::List(){
    headptr=nullptr;
    tailptr=nullptr;
    count=0;
}

Node::Node(int givenId,string givenString){
    searchKey=givenId;
    data=givenString;
    forward=nullptr;
}