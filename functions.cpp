/* ***********************************************************************************************
 * Name: Flavio Palacios                                                                         *
 * Purpose: Functions implementation                                                             *
 * Notes: Functions here are ones that are used solely in ""main.cpp".                           *
 * **********************************************************************************************/

#include "main.h"

//- - - - - - - - - - - - - - -
//   ***TRANSFORM A BOOL***
//- - - - - - - - - - - - - - -
string successEcho(bool success){
    string successString;
    if(success==true){successString="Yes";}
    else{successString="No";};
    return successString;
}

//- - - - - - - - - - - - - - - - -
//   ***FIND CSV FILE'S ROWS***
//- - - - - - - - - - - - - - - - -
int findFileRows(){
    //Variables needed to read the file.
    fstream myFile;
    string fileLine;
    char fileChar;
    int significantLinesInFile=0;

    //The ".csv" file is opened for reading only.
    myFile.open("data.csv",ios::in);

    //File open operation is checked.
    if(!myFile){
        cout<<"ERROR: Action to open file failed."<<endl;
        return significantLinesInFile;
    }
    else{
        //First, the file is traversed with the purpose of counting all lines that
        //are not '\n' or the 'EOF' element. This integer is then stored
        //in the "significantLinesInFile" variable.
        fileChar=myFile.peek();
        while(myFile){
            while(fileChar=='\n'){
                myFile.get();
                fileChar=myFile.peek();
            };

            if(fileChar==EOF){
                myFile.get();
            }
            else{
                getline(myFile,fileLine,'\n');
                significantLinesInFile++;
                fileChar=myFile.peek();
            };
        };

        //The ".csv" file is then closed.
        myFile.close();

        //The return value represents the amount of cells
        //that a 1-D array should have to store file contents
        //presented in a fashion of exactly two data columns.
        //1 additional cell was included to hold the array's
        //length, this way, returning an array from a function
        //should be less difficult.
        return (significantLinesInFile*2)+1;
    };
}

//- - - - - - - - - - - - - - - - - - - -
//   ***STORE FILE ROWS IN AN ARRAY***
//- - - - - - - - - - - - - - - - - - - -
string *storeFileDataInArray(string arrayForFile[],int arraySize){
    //Variables needed to read the file.
    fstream myFile;
    string fileLine;
    char fileChar;

    //File open operation is checked.
    if(!myFile){
        cout<<"ERROR: Action to open file failed."<<endl;
        return nullptr;
    }
    else{
        //All significant elements are stored in the "arrayForFile[]". The zeroth
        //index in this array contains the array's size. This size is also including
        //the zeroth cell.
        arrayForFile[0]=to_string(arraySize);

        myFile.open("data.csv",ios::in);

        int accumulator=1;
        int evenOrOdd;
        while(myFile){
            while(fileChar=='\n'){
                myFile.get();
                fileChar=myFile.peek();
            };

            if(fileChar==EOF){
                myFile.get();
            }
            else{
                evenOrOdd=accumulator%2;
                if(evenOrOdd==0){
                    getline(myFile,fileLine,'\n');
                }
                else{
                    getline(myFile,fileLine,',');
                };
                arrayForFile[accumulator]=fileLine;
                fileChar=myFile.peek();
            };
            accumulator++;
        };

        //For the second and final time, the same file is closed.
        myFile.close();
        return arrayForFile;
    };
}